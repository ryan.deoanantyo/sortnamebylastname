package sort.name.list;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class SortNameBasedLastName {

private Integer dataLenght;
	
	public ArrayList<String> getDataFromTxt() {
		ArrayList<String> dirtyData = new ArrayList<String>(); 
		try {
			RandomAccessFile file = new RandomAccessFile("D:/workspace/name-sorter/unsorted-names-list.txt", "r"); 
			try {
				String str; 
				while ((str = file.readLine()) != null) 
				{ 
					dirtyData.add(str);
				}
				file.close();
			}catch (IOException e) {
				// TODO: handle exception
			}
		}catch (FileNotFoundException fe) {
			// TODO: handle exception
		}
		return dirtyData;
	}
	
	public String getLastName(String name) {
		String lastName = "";
		String[] splitNameBySpace = name.split(" ");
		lastName = splitNameBySpace[splitNameBySpace.length-1];
		return lastName;
	}
	
	public TreeMap<String,String> sortListOfName() {
		HashMap<String,String> unsortedList = new HashMap<String,String>();
		dataLenght = getDataFromTxt().size();
		for(int index=0; index<dataLenght; index++) {
			String lastName = getLastName((String) getDataFromTxt().get(index));
			unsortedList.put(lastName, (String) getDataFromTxt().get(index));
		}
		
		TreeMap<String,String> sortedList = new TreeMap<String,String>(unsortedList);
		return sortedList;
	}
	
	public void printSortedList(TreeMap<String,String> sortedList) {
		File file = new File("D:/workspace/name-sorter/sorted-names-list.txt");
        FileWriter fr = null;
        BufferedWriter br = null;
        String nameWithNewLine = "";
        try{
            fr = new FileWriter(file);
            br = new BufferedWriter(fr);
            for(String lastName : sortedList.keySet()) {
				nameWithNewLine = sortedList.get(lastName) + System.getProperty("line.separator");
				br.write(nameWithNewLine);
				System.out.println(nameWithNewLine);
			}
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		System.out.println("Print sucessfuly!");
	}
	
	public static void main(String[] args) {
		SortNameBasedLastName sortFunction = new SortNameBasedLastName();
		sortFunction.printSortedList(sortFunction.sortListOfName());
	}

}
